# Installation

### tornado
    pip install tornado

### Twitter sdk
    git clone https://github.com/tweepy/tweepy.git
    cd tweepy
    sudo python setup.py install

### Facebook sdk
    sudo pip install facebook-sdk

### aylien sdk for topic analysis
    sudo pip install --upgrade aylien-apiclient

### Social Connector
    git clone https://bitbucket.org/htedsv/socialconnector

# Configuration

### DNS config
    sudo vi /etc/hosts
Add "127.0.0.1  htedsv.appspot.com" at the end of the file.

### Run
    cd socialconnector
    python main.py

### Test
    Open broswer and type "htedsv.appspot.com:8888"

from config import *
from aylienapiclient import textapi
import facebook


# score =   (confidence - 0.5) * subjective + 0.5       if positive
#           1 - ((confidence - 0.5) * subjective + 0.5)   else
def analysis(text):
    client = textapi.Client(AYLIEN_APP_ID, AYLIEN_APP_KEY)
    result = client.Sentiment(text)
    sub = result[u'subjectivity_confidence'] if result['subjectivity'] == u'subjective' else 1.0 - result[u'subjectivity_confidence']
    score = (result[u'polarity_confidence'] - 0.5)*sub + 0.5
    score = score if result['polarity'] == u'positive' else (0.5 if result['polarity'] == u'neutral' else 1 - score)
    return score*100


def topics(text):
    client = textapi.Client(AYLIEN_APP_ID, AYLIEN_APP_KEY)
    return [x[u'label'] for x in client.Classify({'text': text})[u'categories']]

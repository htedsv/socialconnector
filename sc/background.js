document.write("<script src='jquery-2.1.4.min.js'></script>");
document.write("<script src='js.cookie.js'></script>");
document.write("<script src='sha512.js'></script>");
document.write("<script src='LearningContext.js'></script>");
document.write("<script src='LearningContextUtil.js'></script>");    
document.write("<script src='TokenManager.js'></script>");           
document.write("<script src='ContextData.js'></script>");            
document.write("<script src='Entity.js'></script>");
document.write("<script src='Event.js'></script>");

var fb_token;
var tw_token;
var ln_token;
var tw_secret;
var posts;
var interests;
var waiting = false;

function upload()
{
    var lc = new LearningContext("http://api.learning-context.de",
        4, 
        62, 
        'hqqog7936w327j8rf8i4zotp7v98ekw9c3dcn1jzfkqh02pxal',
        'http://htedsv.appspot.com/upload:8888',
        ln_token);

        scores = JSON.parse(posts);
        console.log(scores);
        for (var i in scores)
        {
            var event = new Event("START", i+" 00:00:00", "MOBILE", "PRIVATE", "EMOTION");
            event.addEntity(new Entity("score", scores[i]));
            var json = "[" + event.toJson() + "]";
            var result = lc.post("events", json);
            if (result["result"] == 0)
                alert("Upload fails for date " + i);
        } 

        
        interests = JSON.parse(interests);
        for (var i in interests)
        {
            var event = new Event("START", i, "MOBILE", "PRIVATE", "LIKE");
            page = "https://www.facebook.com/pages/" + interests[i]["text"].replace(/ /g, '-') + "/" + interests[i]["page"]
            event.addEntity(new Entity("resource", page));
            for (var j in interests[i]["topics"])
                event.addInterest(interests[i]["topics"][j]);
            var json = "[" + event.toJson() + "]";
            var result = lc.post("events", json);
            if (result["result"] == 0)
                alert("Upload fails for date " + i);
        }
        alert("Finish");
}

chrome.cookies.get({url:'http://htedsv.appspot.com:8888', name:'learn_access_token'}, 
        function(cookie){
            ln_token = cookie.value;            
        } );

setInterval(function(){ 
    $.post( "http://htedsv.appspot.com:8888/posts", function( data ) {
        chrome.cookies.set({url:'http://htedsv.appspot.com:8888', name:'post', value: data});
        posts = data;
    });
    }, 3600000);


setInterval(function(){ 
    $.post( "http://htedsv.appspot.com:8888/interest", function( data ) {
        chrome.cookies.set({url:'http://htedsv.appspot.com:8888', name:'interests', value: data});
        interests = data;
    });
    }, 3600000);


setInterval(function(){ 
    upload();
    }, 7200000);

chrome.browserAction.onClicked.addListener(function(tab) {
    if (waiting == false)
    {
        alert("Uploading ...");
        waiting = true;
        $.post( "http://htedsv.appspot.com:8888/posts", function( data ) {
            chrome.cookies.set({url:'http://htedsv.appspot.com:8888', name:'post', value: data});
            posts = data;
            $.post( "http://htedsv.appspot.com:8888/interest", function( data ) {
                chrome.cookies.set({url:'http://htedsv.appspot.com:8888', name:'interests', value: data});
                interests = data;
                upload();
                waiting = false;
            });
        });
    }
});

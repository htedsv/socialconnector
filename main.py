import json
import datetime
import tornado.ioloop
import tornado.web
import facebook
import urllib, urllib2
from aylienapiclient import textapi
from config import *
from helper import *


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        is_tw_login = (True if self.get_cookie('twitter_access_token') and self.get_cookie('twitter_access_token_secret') else False)
        is_fb_login = (True if self.get_cookie('facebook_access_token') else False)
        is_learn_login = (True if self.get_cookie('learn_access_token') else False)
        if is_tw_login and is_fb_login:
            self.redirect('/posts')
        else:
            self.set_cookie('next', '/another')
            self.render('index.html', 
                is_tw_login = is_tw_login,
                is_fb_login = is_fb_login,
                is_learn_login = is_learn_login)


class TwitterLoginHandler(tornado.web.RequestHandler):
    def get(self):
        if self.get_cookie('twitter_access_token') and self.get_cookie('twitter_access_token_secret'):
            if self.get_cookie('next'):
                self.redirect(self.get_cookie('next'))
            else:
                self.redirect('/posts')
        else:
            twitter_auth = tweepy.OAuthHandler(TWITTER_KEY, TWITTER_SECRET)
            url = twitter_auth.get_authorization_url()
            rt = twitter_auth.request_token
            self.set_cookie('twitter_oauth_token_secret',       rt['oauth_token_secret'])
            self.set_cookie('twitter_oauth_token',              rt['oauth_token'])
            self.set_cookie('twitter_oauth_callback_confirmed', rt['oauth_callback_confirmed'])
            self.redirect(url)


class TwitterCallbackHandler(tornado.web.RequestHandler):
    def get(self):
        twitter_auth = tweepy.OAuthHandler(TWITTER_KEY, TWITTER_SECRET)
        rq = {}
        rq['oauth_token_secret'] =          self.get_cookie('twitter_oauth_token_secret')
        rq['oauth_token'] =                 self.get_cookie('twitter_oauth_token')
        rq['oauth_callback_confirmed'] =    self.get_cookie('twitter_oauth_callback_confirmed')
        twitter_auth.request_token = rq
        self.request_token = rq
        access_token, access_token_secret = twitter_auth.get_access_token(self.get_argument('oauth_verifier'))
        self.set_cookie('twitter_access_token', access_token)
        self.set_cookie('twitter_access_token_secret', access_token_secret)
        if self.get_cookie('next'):
            self.redirect(self.get_cookie('next'))
        else:
            self.redirect('/posts')


class AnotherAccountHandler(tornado.web.RequestHandler):
    def get(self):
        is_tw_login = (True if self.get_cookie('twitter_access_token') and self.get_cookie('twitter_access_token_secret') else False)
        is_fb_login = (True if self.get_cookie('facebook_access_token') else False)
        if (is_tw_login and is_fb_login) or (not is_tw_login and not is_fb_login):
            self.redirect('/')
        else:
            self.set_cookie('next', '/posts')
            self.render('another.html',
                is_tw_login = is_tw_login,
                is_fb_login = is_fb_login)


class PostsHandler(tornado.web.RequestHandler):
    def get(self):
        posts = []
        timeline = []
        if self.get_cookie('twitter_access_token') and self.get_cookie('twitter_access_token_secret'):
            twitter_auth = tweepy.OAuthHandler(TWITTER_KEY, TWITTER_SECRET)
            twitter_auth.set_access_token(self.get_cookie('twitter_access_token'), self.get_cookie('twitter_access_token_secret'))
            api = tweepy.API(twitter_auth)
            timeline = api.user_timeline()
            for i in range(len(timeline)):
                posts.append((i, {'text':timeline[i].text,
                    'source': 'twitter',
                    'date': str(timeline[i].created_at)
                    }))
        
        if self.get_cookie('facebook_access_token'):
            graph = facebook.GraphAPI(self.get_cookie('facebook_access_token'))
            user = graph.get_object("me")
            fb_posts = graph.get_connections(user["id"], "posts")['data']
            for i in range(len(fb_posts)):
                if 'message' in fb_posts[i]:
                    date = fb_posts[i]['created_time'].encode('utf8').replace('T', ' ').replace('+0000', '')
                    posts.append((len(posts), {'text':fb_posts[i]['message'],
                        'source': 'facebook',
                        'date': date
                        }))
        posts.sort(key=lambda x: x[1]['date'])
        posts.reverse()
        self.render('post.html', posts=posts)

    def post(self):
        scores = {}
        timeline = []
        if self.get_cookie('twitter_access_token') and self.get_cookie('twitter_access_token_secret'):
            twitter_auth = tweepy.OAuthHandler(TWITTER_KEY, TWITTER_SECRET)
            twitter_auth.set_access_token(self.get_cookie('twitter_access_token'), self.get_cookie('twitter_access_token_secret'))
            api = tweepy.API(twitter_auth)
            timeline = api.user_timeline()
            for i in range(len(timeline)):
                date = str(timeline[i].created_at)[:10]
                if date not in scores:
                    scores[date] = []
                scores[date].append(analysis(timeline[i].text))
        
        if self.get_cookie('facebook_access_token'):
            graph = facebook.GraphAPI(self.get_cookie('facebook_access_token'))
            user = graph.get_object("me")
            fb_posts = graph.get_connections(user["id"], "posts")['data']
            for i in range(len(fb_posts)):
                if 'message' in fb_posts[i]:
                    date = fb_posts[i]['created_time'].encode('utf8').replace('T', ' ').replace('+0000', '')[:10]
                if date not in scores:
                    scores[date] = []
                scores[date].append(analysis(fb_posts[i]["message"]))
        scores = {key:sum(scores[key])/len(scores[key]) for key in scores}

        self.write(json.dumps(scores))



class FacebookLoginHandler(tornado.web.RequestHandler):
    def get(self):
        if self.get_cookie('facebook_access_token'):
            if self.get_cookie('next'):
                self.redirect(self.get_cookie('next'))
            else:
                self.redirect('/posts')
        else:
            self.redirect(facebook.auth_url(FACEBOOK_APP_ID, 
                      DOMAIN + ":" + str(PORT) + FACEBOOK_REDIRECT,
                      ['user_posts', 'user_likes']))


class FacebookCallbackHandler(tornado.web.RequestHandler):
    def get(self):
        code = self.get_argument('code')
        token = facebook.get_access_token_from_code(
                code, 
                DOMAIN + ":" + str(PORT) + FACEBOOK_REDIRECT, 
                FACEBOOK_APP_ID, 
                FACEBOOK_APP_SECRET)['access_token']
        self.set_cookie('facebook_access_token', token)

        if self.get_cookie('next'):
            self.redirect(self.get_cookie('next'))
        else:
            self.redirect('/posts')


class AnalysisHandler(tornado.web.RequestHandler):
    def post(self):
        #text = self.get_argument('text').encode('utf8')
        #data = urllib.urlencode({'txt': text})
        #headers = {'X-Mashape-Key': 'agBFwBygCbmsh2suhvprqMCrJkgTp1Woz0XjsnRXhxs2WEXwoV',
        #           'Content-Type': 'application/x-www-form-urlencoded',
        #           'Accept': 'application/json'}
        #u = urllib2.Request('https://community-sentiment.p.mashape.com/text/', data, headers)
        #response = urllib2.urlopen(u)
        #result = json.loads(response.read())['result']
        #score = result['confidence'].encode('utf8')
        #sentiment = result['sentiment']
        #if sentiment == 'Negative':
        #    score = 100.0 - float(score)
        #self.write(str(score))
        self.write(str(analysis(self.get_argument('text').encode('utf8'))))
        

class InterestHandler(tornado.web.RequestHandler):
    def get(self):
        if not self.get_cookie('facebook_access_token'):
            self.set_cookie('next', '/interest')
            self.redirect('/facebook_login')
        else:
            graph = facebook.GraphAPI(self.get_cookie('facebook_access_token'))
            user = graph.get_object("me")
            fb_likes = graph.get_connections(user["id"], "likes")['data']
            for x in fb_likes:
                x['created_time'] = datetime.datetime.strptime(x['created_time'], '%Y-%m-%dT%H:%M:%S+0000').strftime('%Y-%m-%d %H:%M:%S')
            return_list = [(x, fb_likes[x]) for x in range(len(fb_likes))]
            self.render('interest.html', likes = return_list)

    def post(self):
        if not self.get_cookie('facebook_access_token'):
            self.set_cookie('next', '/interest')
            self.redirect('/facebook_login')
        else:
            graph = facebook.GraphAPI(self.get_cookie('facebook_access_token'))
            user = graph.get_object("me")
            fb_likes = graph.get_connections(user["id"], "likes")['data']
            interests = {}
            for x in fb_likes:
                date = datetime.datetime.strptime(x['created_time'], '%Y-%m-%dT%H:%M:%S+0000').strftime('%Y-%m-%d %H:%M:%S')
                interests[date] = {
                        'text':x['name'], 
                        'topics': topics(x['name']),
                        'page': x['id']
                        }
            self.write(json.dumps(interests))


class TopicsHandler(tornado.web.RequestHandler):
    def post(self):
        categories = topics(self.get_argument('text').encode('utf8'))
        self.write('\n'.join(categories))


class UploadHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('upload.html',
                LEARN_VERSION=LEARN_VERSION,
                LEARN_ID=LEARN_ID,
                LEARN_KEY=LEARN_KEY,
                LEARN_REFRESH_TOKEN=self.get_cookie('learn_access_token')
                )


class LearnHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('learn_login.html', 
                LEARN_VERSION=LEARN_VERSION,
                LEARN_ID=LEARN_ID,
                LEARN_KEY=LEARN_KEY)


class LearnCallbackHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_cookie('learn_access_token', self.get_argument('rt'))
        print self.get_argument('rt')
        self.redirect('/')


application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/twitter_login", TwitterLoginHandler),
        (r"/facebook_login", FacebookLoginHandler),
        (r"/posts", PostsHandler),
        (TWITTER_REDIRECT, TwitterCallbackHandler),
        (FACEBOOK_REDIRECT, FacebookCallbackHandler),
        (r"/another", AnotherAccountHandler),
        (r"/analysis", AnalysisHandler),
        (r"/topics", TopicsHandler),
        (r"/interest", InterestHandler),
        (r"/learning", LearnHandler),
        (r"/learning_callback", LearnCallbackHandler),
        (r"/upload", UploadHandler),
        ], template_path = 'templates/',
        static_url = 'static/',
        static_path = 'static/',
        )


if __name__ == "__main__":
    application.listen(PORT)
    tornado.ioloop.IOLoop.current().start()

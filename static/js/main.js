var posts = 0;
var posts_left = 0;
var interests = 0;
var interests_left = 0;
var scoreByDay = {};
var topics = {};

function virsualize(data)
{
    document.getElementById("chartContainer").innerHTML = "";
    var data_arr = [];
    for (var i in data)
        data_arr.push({"date": i, "score":data[i]});
    data = data_arr;
    console.log(data);
    var svg = dimple.newSvg("#chartContainer", 590, 400);
    var myChart = new dimple.chart(svg, data);
    myChart.setBounds(60, 30, 510, 305)
    var x = myChart.addCategoryAxis("x", "date");
    x.addOrderRule("date");
    myChart.addMeasureAxis("y", "score");
    myChart.addSeries(null, dimple.plot.bar);
    myChart.draw();
}

function word_cloud(topic)
{
    wordScale=d3.scale.linear().domain([1,100,1000,10000]).range([10,20,40,80]).clamp(true);
    wordColor=d3.scale.linear().domain([10,20,40,80]).range(["blue","green","orange","red"]);

	viz = d3.select("#viz");


  d3.layout.cloud().size([667, 485])
//      .words([{"text":"test","size":wordScale(.01)},{"text":"bad","size":wordScale(1)}])
      .words(topic)
      .rotate(function() { return ~~(Math.random() * 2) * 5; })
      .fontSize(function(d) { return wordScale(d.size); })
      .on("end", draw)
      .start();

  function draw(words) {
	  
	viz = d3.select("#viz");
        
      viz.append("g")
        .attr("transform", "translate(200,220)")
      .selectAll("text")
        .data(words)
      .enter().append("text")
        .style("font-size", function(d) { return d.size + "px"; })
        .style("fill", function(d) { return wordColor(Math.random()*80); })
        .style("opacity", .75)
        .attr("text-anchor", "middle")
        .attr("transform", function(d) {
          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function(d) { return d.text; });
    
		viz
        .append("text")
        .data([topic[0]])
        .style("font-size", 20)
        .style("font-weight", 900)
        .attr("x", 100)
        .attr("y", 20)
     //   .text(function(d) { return "TOPIC " + d.topic; })

//  d3.select("#svg"+x).append("svg:text").text("Topic " + x);	
//    viz.enter().append("svg:text").text("Topic " + x);

  }
}

function ave(arr)
{
    var sum = 0;
    for (var i = 0; i < arr.length; ++i)
        sum += Number(arr[i]);
    return sum * 1.0 / arr.length;
}

function group()
{
    var post_arr = new Array();

    for (var i = 0; i < posts; i++)
    {
        var date = document.getElementById('date'+i).innerHTML.substring(0, 10);
        var score = document.getElementById(i+'').innerHTML;

        if (!(date in post_arr))
            post_arr[date] = new Array();
        post_arr[date].push(score);
    }
    for (var i in post_arr)
    {
        scoreByDay[i] = ave(post_arr[i]);
    }
    virsualize(scoreByDay);
    Cookies.set('post', JSON.stringify(scoreByDay));
}

function analysis(text, id)
{
    $.post("/analysis", {'text': text}, 
            function(result){
                document.getElementById(id).innerHTML=result;
                posts_left = posts_left - 1;
                if (posts_left == 0)
                    group();
            });
}

function topics_pool()
{
    Cookies.set('interests', JSON.stringify(topics));
    var topics_pool = [];
    for (var i in topics)
        for (var j = 0; j < topics[i]["topics"].length; ++j)
            topics_pool = topics_pool.concat(topics[i]["topics"][j].split(" "));
    var topics_with_scale = []
    for (var i = 0; i < topics_pool.length; ++i)
        topics_with_scale.push({"text": topics_pool[i], "size": "300"});

    word_cloud(topics_with_scale);
}

function topics_get(text, topic_id, date_id, page_id)
{
    $.post("/topics", {'text': text}, 
            function(result){
                document.getElementById(topic_id).innerHTML=result;
                var date = document.getElementById(date_id).innerHTML;
                var page = document.getElementById(page_id).innerHTML;
                topics[date] = {"text": text, "topics": result.split('\n'), "page": page};
                interests_left -= 1;
                if (interests_left == 0)
                    topics_pool();
            });
}

function explore()
{
    var lc = new LearningContext("http://api.learning-context.de",
        4, 
        62, 
        'hqqog7936w327j8rf8i4zotp7v98ekw9c3dcn1jzfkqh02pxal',
        'http://htedsv.appspot.com/upload:8888',
        Cookies.get('learn_access_token'));
    var result = lc.get("events", '{"model":"COMPLETE"}');
    console.log(result); 
}

function clean()
{
    var lc = new LearningContext("http://api.learning-context.de",
        4, 
        62, 
        'hqqog7936w327j8rf8i4zotp7v98ekw9c3dcn1jzfkqh02pxal',
        'http://htedsv.appspot.com/upload:8888',
        Cookies.get('learn_access_token'));
    var result = lc.get("events", '{"model":"COMPLETE"}');
    console.log(result); 

}

function upload()
{
    var lc = new LearningContext("http://api.learning-context.de",
        4, 
        62, 
        'hqqog7936w327j8rf8i4zotp7v98ekw9c3dcn1jzfkqh02pxal',
        'http://htedsv.appspot.com/upload:8888',
        Cookies.get('learn_access_token'));

        scores = JSON.parse(Cookies.get('post'))
        for (var i in scores)
        {
            var event = new Event("START", i+" 00:00:00", "MOBILE", "PRIVATE", "EMOTION");
            event.addEntity(new Entity("score", scores[i]));
            var json = "[" + event.toJson() + "]";
            var result = lc.post("events", json);
            if (result["result"] == 0)
                alert("Upload fails for date " + i);
        } 

        
        interests = JSON.parse(Cookies.get('interests'))
        for (var i in interests)
        {
            var event = new Event("START", i, "MOBILE", "PRIVATE", "LIKE");
            page = "https://www.facebook.com/pages/" + interests[i]["text"].replace(/ /g, '-') + "/" + interests[i]["page"]
            event.addEntity(new Entity("resource", page));
            for (var j in interests[i]["topics"])
                event.addInterest(interests[i]["topics"][j]);
            var json = "[" + event.toJson() + "]";
            var result = lc.post("events", json);
            if (result["result"] == 0)
                alert("Upload fails for date " + i);
        }
        alert("Finish");
}

